'''
Aim:

Given an integer k and a string s, find the length of the longest substring that contains at most k distinct characters.

For example, given s = "abcba" and k = 2, the longest substring with k distinct characters is "bcb".

'''


def get_substring(str_name,k):
	str_list = []
	for i in reversed(range(len(str_name)+1)):
		for index,x in enumerate(str_name):
			str_list.append(str_name[index:index+i])

	filtered_list = sorted(list(filter(None,str_list)),key=len,reverse=True)
	for i in filtered_list:
		if len(set(list(i))) == k:
			return(i)


#print(get_substring('abcba', 2))

@pytest.mark.parametrize('input1,input2,output1', [('abcba',2,'bcb'), ('erhre',2,'rhr')])
def test_function(input1,input2,output1):
    assert get_substring(input1,input2) == output1
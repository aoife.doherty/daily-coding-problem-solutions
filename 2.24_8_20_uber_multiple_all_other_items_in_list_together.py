from functools import reduce

'''
Aim: Given an array of integers, return a new array such that each element at index i of the new array is the product of all the numbers in the original array except the one at i.

For example, if our input was [1, 2, 3, 4, 5], the expected output would be [120, 60, 40, 30, 24]. If our input was [3, 2, 1], the expected output would be [2, 3, 6].
'''


import numpy as np
from functools import reduce

"""
Aim: Given an array of integers, return a new array such that each element at index i of the new array is the product of all the numbers in the original array except the one at i.

For example, if our input was [1, 2, 3, 4, 5], the expected output would be [120, 60, 40, 30, 24]. If our input was [3, 2, 1], the expected output would be [2, 3, 6].
"""
input1 = [1, 2, 3, 4, 5]
output1 = [120, 60, 40, 30, 24]
input2 = [3, 2, 1]
output2 = [2, 3, 6]


def return_product_of_all_integers_v1(list_name):
    new_list = []
    for index, i in enumerate(list_name):
        current_list = [i for x, i in enumerate(list_name) if x != index]
        new_list.append(reduce(lambda x, y: x * y, current_list))
    return new_list


def return_product_of_all_integers_v2(list_name):
    all_mult = np.prod(list_name)
    return [all_mult / c for c in list_name]


def test_return_product_of_all_integers_v1():
    assert return_product_of_all_integers_v1(input1) == output1
    assert return_product_of_all_integers_v1(input2) == output2


def test_return_product_of_all_integers_v2():
    assert return_product_of_all_integers_v2(input1) == output1
    assert return_product_of_all_integers_v2(input2) == output2

'''
Aim:

Given the mapping a = 1, b = 2, ... z = 26, and an encoded message, count the number of ways it can be decoded.

For example, the message '111' would give 3, since it could be decoded as 'aaa', 'ka', and 'ak'.

You can assume that the messages are decodable. For example, '001' is not allowed.

'''

import pytest

def splitter(str):
    for i in range(1, len(str)):
        start = str[0:i]
        end = str[i:]
        yield (start, end)
        for split in splitter(end):
            result = [start]
            result.extend(split)
            yield result


def get_len(str_name):
      combinations = list(map(list,list(splitter(str_name))))
      int_list = [[int(i) for i in combo] for combo in combinations]
      filtered_list = [i for i in int_list if all(x < 27 for x in i)]
      return len(filtered_list)




@pytest.mark.parametrize("input1, output1", [('1212123', 21), ('12132221249', 65)])
def test_count(input1,output1):
    assert get_len(input1) == output1



##Method 2
#Could also use itertools, e.g. something like:

#import more_itertools as mit
#s = '11121'
#lists = mit.partitions(s)
#for i in lists:
#        print(i)
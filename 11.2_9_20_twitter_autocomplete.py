import ast
import pytest
import re
from collections import OrderedDict


def autocomplete1(str,list_name):
    return [i for i in list(OrderedDict.fromkeys(list_name)) if i.startswith(str)]


def autocomplete2(str,list_name):
    return(list(filter(lambda x: x.startswith(str), list(OrderedDict.fromkeys(list_name)))))


def autocomplete3(str,list_name):
    return([i for i in list_name if re.match(str,i)])


#fix the list
@pytest.mark.parametrize('input1, input2, output1', [('de',['dog','deer','deal'],['deer','deal']), ('ear',['earplug','earphone','airplane'],['earplug','earphone'])])
def test_function(input1,input2,output1):
    assert autocomplete1(input1,input2) == output1
    assert autocomplete2(input1,input2) == output1
    assert autocomplete3(input1,input2) == output1
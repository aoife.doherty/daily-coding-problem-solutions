'''
Question:
cons(a, b) constructs a pair, and car(pair) and cdr(pair) returns the first and last element of that pair. For example, car(cons(3, 4)) returns 3, and cdr(cons(3, 4)) returns 4.

Answer:
As with pytest, run: python3 -m pytest test.py
'''


import pytest

def cons(a, b):
    return (a, b)


def car(tuple_name):
    return tuple_name[0]


def cdr(tuple_name):
    return tuple_name[1]


@pytest.mark.parametrize("input1, output1", [((3, 4), 3), ((5, 4), 5)])
def test_car(input1, output1):
    assert car(input1) == output1
'''
Aim:
Given an array of integers, find the first missing positive integer in linear time and constant space. In other words, find the lowest positive integer that does not exist in the array. The array can contain duplicates and negative numbers.


To do:
More testing
'''

def print_next_positive_integer_in_list(l):
        my_list = [i for i in sorted(list(set(l))) if i >= 0]
        ideal_list = list(range(min(my_list),max(my_list)+1))
        if my_list == ideal_list:
                return my_list[-1] + 1
        else:
                for x,y in zip(my_list,ideal_list):
                        if x != y:
                                return(y)
                                break

list1 = [0,-3,5,3,6,6]
print(print_next_positive_integer_in_list(list1))


